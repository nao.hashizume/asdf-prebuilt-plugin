#!/usr/bin/env bash

set -euo pipefail

# shellcheck source=../lib/utils.sh
source "$(dirname $0)/../lib/utils.sh"

install_package() {
  local install_type="${1}"
  local version="${2}"
  local install_path="${3}"

  if [ "${TMPDIR:-}" = "" ]; then
    local tmp_download_dir=$(mktemp -d)
  else
    local tmp_download_dir=$TMPDIR
  fi

  local package_path=$(get_download_file_path $version $tmp_download_dir)

  # echo "DEBUG: install_type=[${install_type}], version=[${version}], install_path=[${install_path}], tmp_download_dir=[${tmp_download_dir}], package_path=[${package_path}]" >&2

  download_package $version $package_path

  (
    mkdir -p ${install_path}
    cd ${install_path}
    tar xzf ${package_path}
  )
}

download_package() {
  local version="${1}"
  local download_path="${2}"

  local download_url=$(get_download_url $version)

  # ensure any previously downloaded tar ball is valid
  if ! tar tzf $download_path > /dev/null 2>&1; then
    rm -f $download_path
  fi

  curl -Lo $download_path -C - $download_url
}

get_download_file_path() {
  local version="${1}"
  local tmp_download_dir="${2}"

  local platform="$(get_platform)"
  local arch="$(get_arch)"
  local pkg_name="${PLUGIN_NAME}_${version}_${platform}_${arch}.tar.gz"

  echo "$tmp_download_dir/$pkg_name"
}

get_download_url() {
  local version="${1}"
  local platform="$(get_platform)"
  local arch="$(get_arch)"
  local timestamp="$(get_timestamp)"
  release="$(get_release)"
  local download_url="${BASE_URL}/${platform}/${release}/${arch}/${PLUGIN_NAME}/${PLUGIN_NAME}_${version}_${platform}_${arch}.tar.gz?${timestamp}"

  # echo "DEBUG: download_url=[${download_url}]" >&2

  echo "${download_url}"
}

install_package $ASDF_INSTALL_TYPE $ASDF_INSTALL_VERSION $ASDF_INSTALL_PATH
